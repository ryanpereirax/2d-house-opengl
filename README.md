# 2D House Opengl

![house_end.png](house_end.png)

## Getting started

Dependencies


##### Linux

```bash
sudo apt install g++ libglu1-mesa-dev freeglut3-dev mesa-common-dev
```

## Add your files

- [ ] [Create](https://gitlab.com/-/experiment/new_project_readme_content:e6831484c511b124990fbf1d8e9ebbb0?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://gitlab.com/-/experiment/new_project_readme_content:e6831484c511b124990fbf1d8e9ebbb0?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://gitlab.com/-/experiment/new_project_readme_content:e6831484c511b124990fbf1d8e9ebbb0?https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/rodpwn/2d-house-opengl.git
git branch -M main
git push -uf origin main
```

## 2D House OpenGL ( Unicap )


## Description
project using the opengl library for the subject of computer graphics at the catholic university of Pernambuco

## Usage
Compiling using your IDE, DEVC++, Eclipse or Intelij

Using terminal:

```bash
g++ projeto_01.cpp -o app -lglut -lGLU -lGL
```

## Authors and acknowledgment
```
Rodolfo Tavares
Ryan Pereira
Lucas Mancilha
Jose Aroldo
```
## License
For open source projects, say how it is licensed.

