#ifdef __linux__ 
    #include <GL/glut.h>
    #include <stdlib.h>
    #include <stdio.h>
    #include <iostream>
    #include <math.h>
#elif _WIN32
    #include <windows.h> 
    #include <GL/glut.h>
    #include <iostream>
    #include <stdlib.h>
    #include <stdio.h>
    #include <math.h>
#else
    #include <GL/glut.h>
    #include <iostream>
    #include <stdlib.h>
    #include <stdio.h>
    #include <math.h>
#endif

#define M_PI 3.14159265358979323846

float Ty = 0.0;
float Tx = 0.0;
float Tr = 0.0;
float Cr = 1.0;
float Cg = 0.0;
float Cb = 0.0;
float Px = 0.0;
float roofX = 0.0;


void house2 (void) {
    glBegin(GL_QUADS); //roof
 glColor3f(0.1,0.9,1);
 glVertex3f(0.5, 0.4, 0);
 glVertex3f(-0.3, 0.4, 0);
 glVertex3f(-0.5,0.0, 0);
 glVertex3f(0.3, 0.0, 0);
 glEnd();//end the shape we are currently working on
 
 
    glBegin(GL_TRIANGLES); //side roof
 glColor3f(0.38,0.52,1);
 glVertex3f(0.5, 0.4, 0);
 glVertex3f(0.3, 0.0, 0);
 glVertex3f(0.6,0.15, 0);
 glEnd();
 
 glBegin(GL_QUADS); //wall
 glColor3f(1,0.87,0.87);
 glVertex3f(0.3, 0.0, 0);
 glVertex3f(0.3, -0.4, 0);
 glVertex3f(-0.4,-0.4, 0);
 glVertex3f(-0.4, 0.0, 0);
 glEnd();
 
 glBegin(GL_QUADS); //side wall
 glColor3f(0.57,0.87,0.87);
 glVertex3f(0.6, 0.15, 0);
 glVertex3f(0.6, -0.2, 0);
 glVertex3f(0.3,-0.4, 0);
 glVertex3f(0.3, 0.0, 0);
 glEnd();
 
 glBegin(GL_QUADS); //door frame
 glColor3f(0.1,0.5,0.8);
 glVertex3f(-0.1,-0.2, 0);
 glVertex3f(-0.1,-0.4, 0);
 glVertex3f(0.1,-0.4, 0);
 glVertex3f(0.1,-0.2, 0);
 glEnd();
 glBegin(GL_QUADS); //door upper
 glColor3f(0,0,0);
 glVertex3f(-0.1,-0.2, 0);
 glVertex3f(-0.033,-0.25, 0);
 glVertex3f(0.033,-0.25, 0);
 glVertex3f(0.1,-0.2, 0);
 glEnd();
 glBegin(GL_QUADS); //door lower
 glColor3f(0,0,0);
 glVertex3f(-0.033,-0.25, 0);
 glVertex3f(-0.033,-0.4, 0);
 glVertex3f(0.033,-0.4, 0);
 glVertex3f(0.033,-0.25, 0);
 glEnd();
 glBegin(GL_QUADS); //left window
 glColor3f(0,0,0);
 glVertex3f(-0.3,-0.1, 0);
 glVertex3f(-0.3,-0.3, 0);
 glVertex3f(-0.15,-0.3, 0);
 glVertex3f(-0.15,-0.1, 0);
 glEnd();
 glBegin(GL_QUADS); //right window
 glColor3f(0,0,0);
 glVertex3f(0.15,-0.1, 0);
 glVertex3f(0.15,-0.3, 0);
 glVertex3f(0.28,-0.3, 0);
 glVertex3f(0.28,-0.1, 0);
 glEnd();
 glBegin(GL_QUADS); //side window
 glColor3f(0,0,0);
 glVertex3f(0.5,0.0, 0);   // upper right
 glVertex3f(0.4,-0.05, 0); // upper left
 glVertex3f(0.4,-0.2, 0);  // lower left
 glVertex3f(0.5,-0.15, 0); // lower right
 glEnd();
 glBegin(GL_QUADS); //lower part
 glColor3f(1,0.45,0.25);
 glVertex3f(0.3,-0.4, 0);   // upper right
 glVertex3f(-0.4,-0.4, 0); // upper left
 glVertex3f(-0.4,-0.45, 0);  // lower left
 glVertex3f(0.3,-0.45, 0); // lower right
 glEnd();
 glBegin(GL_QUADS); //side lower part
 glColor3f(1,0.45,0.45);
 glVertex3f(0.6,-0.2, 0);   // upper right
 glVertex3f(0.3,-0.4, 0); // upper left
 glVertex3f(0.3,-0.45, 0);  // lower left
 glVertex3f(0.6,-0.25, 0); // lower right
 glEnd();
 
}



void drawHouse() {

	//side
	glColor3f(0.1, 0.1, 1.0);
	glLineWidth(5.0);
	glBegin(GL_QUADS);
	glVertex3f(0.1f, -0.5f, -5.0f);
	glVertex3f(0.1f, -1.5, -5.0f);
	glVertex3f(1.5f, -1.5f, -5.0f);
	glVertex3f(1.5f, -0.5f, -5.0f);
	glEnd();

	//roof
	glLineWidth(5.0);
	glColor3ub(229.0, 229.0, 214.0);
	glBegin(GL_TRIANGLES);
	glVertex3f(-0.8f, -0.5f, -5.0f);
	glVertex3f(-0.3f, 0.5f, -5.0f);
	glVertex3f(0.10f, -0.5f, -5.0f);
	glEnd();
	//roof2
	glLineWidth(5.0);
	glColor3ub(98.0, 98.0, 8.0);
	glBegin(GL_QUADS);
	glVertex3f(-0.3f, 0.5f, -5.0f);
	glVertex3f(0.15f, -0.6f, -5.0f);
	glVertex3f(1.60f, -0.6f, -5.0f);
	glVertex3f(1.10f, 0.5f, -5.0f);
	glEnd();

	

	//window1
	glColor3f(0.80, 0.70, 0.60);
	glBegin(GL_QUADS);
	glVertex3f(0.35, -0.8, -5.0);
	glVertex3f(0.35, -1.1, -5.0);
	glVertex3f(0.75, -1.1, -5.0);
	glVertex3f(0.75, -0.8, -5.0);
	glEnd();
	glLineWidth(2.0);
	glBegin(GL_LINES);
	glColor3f(0.0, 0.0, 0.0);
	glVertex3f(0.35, -0.8, -5.0);
	glVertex3f(0.75, -1.1, -5.0);
	glEnd();
	//window2
	glColor3f(0.80, 0.70, 0.60);
	glBegin(GL_QUADS);
	glVertex3f(0.85, -0.8, -5.0);
	glVertex3f(0.85, -1.1, -5.0);
	glVertex3f(1.25, -1.1, -5.0);
	glVertex3f(1.25, -0.8, -5.0);
	glEnd();
	glLineWidth(2.0);
	glBegin(GL_LINES);
	glColor3f(0.0, 0.0, 0.0);
	glVertex3f(0.85, -0.8, -5.0);
	glVertex3f(1.25, -1.1, -5.0);
	glEnd();
}

void cloud(float rad, float xx, float yy) {

	float thetha = 2 * 3.1415 / 20;
	float x, y;
	glColor3f(1.1, 1.1, 1.10);
	glLineWidth(1.0);
	glBegin(GL_TRIANGLE_FAN);
	for (int i = 0; i < 20; i++) {
		x = rad * cos(i*thetha) + xx;
		y = rad*sin(i*thetha) + yy;
		float z = -5.0f;
		glVertex3f(x, y, z);
	}
	glEnd();
}


void road()
{
	glColor3ub(0, 0, 0);
	glLineWidth(50.0);
	glBegin(GL_QUADS);
	glVertex3f(-1.5f, -1.8f, -5.0f);
	glVertex3f(-1.6f, -2.1f, -5.0f);
	glVertex3f(0.5f, -2.1f, -5.0f);
	glVertex3f(0.6f, -1.80f, -5.0f);
	glEnd();
}

void think()
{
	glColor3f(1.1, 1.1, 1.1);
	cloud(0.8, -1.90f, .97f);
	cloud(0.8, -1.4f, 0.97f);
	cloud(0.8, -3.0f, 0.97f);
	cloud (0.8, -3.5f, 0.67f);
	glColor3ub(25, 10, 91);
	glRasterPos3f(-3.5, 1.5, -5.0);
}


void circle(float rad, float xx, float yy) {

	float thetha = 2 * 3.1415 / 20;
	float x, y;
	glColor3f(1.1, 1.1, 1.10);
	glLineWidth(1.0);
	glBegin(GL_LINE_LOOP);
	for (int i = 0; i < 20; i++) {
		x = rad * cos(i*thetha) + xx;
		y = rad*sin(i*thetha) + yy;
		float z = -5.0f;
		glVertex3f(x,y,z);
	}
	glEnd();
}


void DefineSquare() {
    glBegin(GL_QUADS);
        glVertex2f(0.3, 0.0);
        glVertex2f(0.85, 0.0);
        glVertex2f(0.85, -0.6);
        glVertex2f(0.3, -0.6);
    glEnd();
}

void DefineFloor() {
     glBegin(GL_QUADS);
         glColor3f(0.1f, 0.1f, 0.0f);
         glVertex2f(-1.0, -0.6);
         glVertex2f(-1.0, -1.5);
         glVertex2f(1.0, -0.6);
         glVertex2f(1.0, -1.5);
     glEnd();
}

void DefineDoor() {
     glBegin(GL_QUADS);
         glColor3f(0.1f, 0.1f, 0.0f);
         glVertex2f(0.4, -0.6);
         glVertex2f(0.4, -0.3 + Px);
         glVertex2f(0.5 + Px, -0.3 + Px);
         glVertex2f(0.5 + Px , -0.6);
     glEnd();
}

void DefineWindow01() {
     glBegin(GL_LINE_STRIP);
     glColor3f(0.0f, 0.0f, 0.0f);
     glVertex2f(0.6, -0.1);
     glVertex2f(0.7, -0.1);
     glVertex2f(0.8, -0.1);
     glVertex2f(0.8, -0.2);
     glVertex2f(0.8, -0.3);
     glVertex2f(0.7, -0.3);
     glVertex2f(0.6, -0.3);
     glVertex2f(0.6, -0.2);
     glVertex2f(0.6, -0.1);
     glEnd();
}

void DefineMiddleWindow() {
     glBegin(GL_LINE_STRIP);
     glColor3f(0.0f, 0.0f, 0.0f);
     glVertex2f(0.6, -0.2);
     glVertex2f(0.7, -0.2);
     glVertex2f(0.7, -0.1);
     glVertex2f(0.7, -0.3);
     glVertex2f(0.7, -0.2);
     glVertex2f(0.8, -0.2);
     
    glEnd();
}

void DefineStar (void) {
    glBegin(GL_TRIANGLES);
 glColor3f(1,1,1);
 glVertex3f(0.0, 0.6, 0);
 glVertex3f(-0.3, 0.0, 0);
 glVertex3f(0.3,0.0, 0);
 
 glEnd();//end the shape we are currently working on
 glBegin(GL_TRIANGLES);
 glColor3f(1,1,1);
 glVertex3f(0.0, -0.2, 0);
 glVertex3f(-0.3, 0.4, 0);
 glVertex3f(0.3,0.4, 0);
 
 glEnd();//end the shape we are currently working on
}


void DefineTriangle() {
    glBegin(GL_TRIANGLES);
        glColor3f(0.1, 0.2, 0.3);
        glVertex3f(0.2 + roofX, 0, 0);
        glVertex3f(0.8 - roofX, 0, 0);
        glVertex3f(0.5, 0.5, 0);
    glEnd();
}


void DefineSun()
{
    glColor3f(1.0f, 1.0f, 1.0f);
    glBegin(GL_LINE_LOOP);
    for (int i = 0; i <= 300; i++) {
        double angle = 2 * M_PI * i / 300;
        double x = cos(angle);
        double y = sin(angle);
        if (i > 250) {
            glColor3f(0.0f, 1.0f, 0.0f);
        }

        glVertex2d(x, y);
    }
    glEnd();
}

void Draw()
{

    
    glClear(GL_COLOR_BUFFER_BIT); // clear colors of window
   //Drawing start with default from gl.h
    
    glPushMatrix();
        glScalef(0.1, 0.1, 1);
        house2();
    glPopMatrix();
    
    glPushMatrix();
        glColor3f(1.0f, 1.0f, 1.0f);
        glScalef(0.2, 0.2, 0);
        glTranslated(3,3,0.0);
        DefineStar();
        think();
    glPopMatrix();

    // Drawing the Sun
    glPushMatrix();
        glScalef(0.1, 0.1, 0);
        glTranslatef(-5, 6, 0);
        glRotated(Tr, 0, 0,1);
        DefineSun();   
    glPopMatrix();

    glPushMatrix();
        glScalef(0.1, 0.1, 0);
        glTranslatef(-5, 0.0, 0.0);
        drawHouse();
    glPopMatrix();

    // Drawing the House Step by Step :]
    glPushMatrix();
        
        DefineFloor();
        glColor3f(Cr, Cg, Cb);
        glTranslated(Tx, Ty, 0.0);

        // house will be draw from here
        DefineSquare();
        DefineDoor();
        DefineWindow01();
        DefineMiddleWindow();
        // end house draw
        glTranslated(0.07, 0.0, 0.0);
        DefineTriangle();
        
    glPopMatrix();
    glFlush();
}

void keyboard(unsigned char keyboardButton, int mouseXPositions, int mouseYPositions) {
    
    /***
     * 
     *  keyboardButton are actions when a user, press any keys.
     *  mouseXPositions and mouseYPositions are mouse positions when user press any key
     */

    switch (keyboardButton) {
    case 'a':
        Px += 0.001;
        break;
    case 'g':
       Px -= 0.001;
        break;
    case 'd':
        roofX += 0.001;
        break;
    case 'f':
        roofX -= 0.001;
        break;
    case 'r':
        Tr += 50;
        break;
    default:
        break;
    }

    glutPostRedisplay(); // window render >> dangerous function !!
}


void MouseActions(int button, int state, int x, int y) {
    
    /***
     * 
     *  MouseActions are actions when a user, press any keys and
     *  mouse actions will be rendered by theses keys ( buttons )
     * 
     */

    if (button == GLUT_LEFT_BUTTON) {
        if (state == GLUT_DOWN)
            if (Cr == 1) {
                Cr = 0.0;
                Cg = 1.0;
                Cb = 0.0;
            }
            else if (Cg == 1.0) {
                Cr = 0.0;
                Cg = 0.0;
                Cb = 1.0;
            }
            else if (Cb = 1.0) {
                Cr = 1.0;
                Cg = 0.0;
                Cb = 0.0;
            }   
    }
    glutPostRedisplay(); // window render >> dangerous function !!
}

void Skeys(int keyboardKeys, int mouseXposition, int mouseYposition) {
    /***
     * 
     * 
     * 
     * KeyboardKeys are keys that will be selected when user press it 
     * 
     * 
     * mouseXposition and mouseYposition will be the positions of the mouse when any key is pressed
     * 
     *
     */

    switch (keyboardKeys) {
    case GLUT_KEY_UP:
        Ty += 0.1;
        break;
    case GLUT_KEY_DOWN:
        Ty -= 0.1;
        break;
    case GLUT_KEY_RIGHT:
        Tx += 0.2;
        break;
    case GLUT_KEY_LEFT:
        Tx -= 0.2;
        break;
    default:
        break;
    }
    glutPostRedisplay(); // window render >> dangerous function !!
}


void StartEverything(void)
{
    glClearColor(0, 0.1, 0.1, 0); // dark blue background from window ( this is the init window  )
}

/* Program entry point */

int main(int argc, char* argv[])
{
    glutInit(&argc, argv);
    glutInitWindowSize(800, 600);
    glutInitWindowPosition(10, 10);
    glutInitDisplayMode(GLUT_RGB | GLUT_SINGLE);

    glutCreateWindow("Projeto computacao grafica");
    glutDisplayFunc(Draw);
    
    glutSpecialFunc(Skeys); // special keys from keyboard
    glutKeyboardFunc(keyboard); // keyboard actions
    glutMouseFunc(MouseActions); // mouse actions

    StartEverything();
    glutMainLoop();

    return 0;
}